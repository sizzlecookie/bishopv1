Refinery::Search.configure do |config|
   config.enable_for = ["Refinery::Vehicles::Vehicle"]
   # config.page_url = "/search"
   config.results_per_page = 10
end