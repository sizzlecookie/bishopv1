# This migration comes from refinery_vehicles (originally 1)
class CreateVehiclesVehicles < ActiveRecord::Migration

  def up
    create_table :refinery_vehicles do |t|
      t.string :year
      t.string :make
      t.string :model
      t.string :price
      t.text :description
      t.boolean :sold
      t.date :listed_on
      t.date :sold_on
      t.integer :photo1_id
      t.integer :photo2_id
      t.integer :photo3_id
      t.integer :photo4_id
      t.integer :photo5_id
      t.integer :photo6_id
      t.integer :photo7_id
      t.integer :photo8_id
      t.integer :photo9_id
      t.integer :photo10_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-vehicles"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/vehicles/vehicles"})
    end

    drop_table :refinery_vehicles

  end

end
