module Refinery
  module Vehicles
    class Vehicle < Refinery::Core::BaseModel
      acts_as_indexed :fields => [:year, :make, :model, :price, :description]
      alias_attribute :title, :model


      self.table_name = 'refinery_vehicles'


      validates :year, :presence => true, :uniqueness => true

      belongs_to :photo1, :class_name => '::Refinery::Image'

      belongs_to :photo2, :class_name => '::Refinery::Image'

      belongs_to :photo3, :class_name => '::Refinery::Image'

      belongs_to :photo4, :class_name => '::Refinery::Image'

      belongs_to :photo5, :class_name => '::Refinery::Image'

      belongs_to :photo6, :class_name => '::Refinery::Image'

      belongs_to :photo7, :class_name => '::Refinery::Image'

      belongs_to :photo8, :class_name => '::Refinery::Image'

      belongs_to :photo9, :class_name => '::Refinery::Image'

      belongs_to :photo10, :class_name => '::Refinery::Image'

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #

    end
  end
end
