module Refinery
  module Vehicles
    class VehiclesController < ::ApplicationController

      before_action :find_all_vehicles
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @vehicle in the line below:
        present(@page)

        # @vehicles = Vehicle.all
        # if params[:search]
        #   @vehicles = Vehicle.search(params[:search]).order("created_at DESC")
        # else
        #   @vehicles = Vehicle.all.order('created_at DESC')
        # end

      end

      def show
        @vehicle = Vehicle.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @vehicle in the line below:
        present(@page)
      end

    protected

      def find_all_vehicles
        @vehicles = Vehicle.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/vehicles").first
      end
    end
  end
end
