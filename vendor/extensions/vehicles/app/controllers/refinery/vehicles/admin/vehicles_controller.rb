module Refinery
  module Vehicles
    module Admin
      class VehiclesController < ::Refinery::AdminController

        crudify :'refinery/vehicles/vehicle',
                :title_attribute => 'year'

        private

        # Only allow a trusted parameter "white list" through.
        def vehicle_params
          params.require(:vehicle).permit(:year, :make, :model, :price, :description, :sold, :listed_on, :sold_on, :photo1_id, :photo2_id, :photo3_id, :photo4_id, :photo5_id, :photo6_id, :photo7_id, :photo8_id, :photo9_id, :photo10_id)
        end
      end
    end
  end
end
