# Vehicles extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/vehicles
    gem build refinerycms-vehicles.gemspec
    gem install refinerycms-vehicles.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-vehicles.gem
