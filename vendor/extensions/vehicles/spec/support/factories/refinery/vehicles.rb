
FactoryGirl.define do
  factory :vehicle, :class => Refinery::Vehicles::Vehicle do
    sequence(:year) { |n| "refinery#{n}" }
  end
end

