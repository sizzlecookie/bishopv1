# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "Vehicles" do
    describe "Admin" do
      describe "vehicles", type: :feature do
        refinery_login

        describe "vehicles list" do
          before do
            FactoryGirl.create(:vehicle, :year => "UniqueTitleOne")
            FactoryGirl.create(:vehicle, :year => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.vehicles_admin_vehicles_path
            expect(page).to have_content("UniqueTitleOne")
            expect(page).to have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.vehicles_admin_vehicles_path

            click_link "Add New Vehicle"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Year", :with => "This is a test of the first string field"
              expect { click_button "Save" }.to change(Refinery::Vehicles::Vehicle, :count).from(0).to(1)

              expect(page).to have_content("'This is a test of the first string field' was successfully added.")
            end
          end

          context "invalid data" do
            it "should fail" do
              expect { click_button "Save" }.not_to change(Refinery::Vehicles::Vehicle, :count)

              expect(page).to have_content("Year can't be blank")
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:vehicle, :year => "UniqueTitle") }

            it "should fail" do
              visit refinery.vehicles_admin_vehicles_path

              click_link "Add New Vehicle"

              fill_in "Year", :with => "UniqueTitle"
              expect { click_button "Save" }.not_to change(Refinery::Vehicles::Vehicle, :count)

              expect(page).to have_content("There were problems")
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:vehicle, :year => "A year") }

          it "should succeed" do
            visit refinery.vehicles_admin_vehicles_path

            within ".actions" do
              click_link "Edit this vehicle"
            end

            fill_in "Year", :with => "A different year"
            click_button "Save"

            expect(page).to have_content("'A different year' was successfully updated.")
            expect(page).not_to have_content("A year")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:vehicle, :year => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.vehicles_admin_vehicles_path

            click_link "Remove this vehicle forever"

            expect(page).to have_content("'UniqueTitleOne' was successfully removed.")
            expect(Refinery::Vehicles::Vehicle.count).to eq(0)
          end
        end

      end
    end
  end
end
