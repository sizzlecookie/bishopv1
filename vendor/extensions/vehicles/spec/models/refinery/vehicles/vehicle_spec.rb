require 'spec_helper'

module Refinery
  module Vehicles
    describe Vehicle do
      describe "validations", type: :model do
        subject do
          FactoryGirl.create(:vehicle,
          :year => "Refinery CMS")
        end

        it { should be_valid }
        its(:errors) { should be_empty }
        its(:year) { should == "Refinery CMS" }
      end
    end
  end
end
