module Refinery
  module Vehicles
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Vehicles

      engine_name :refinery_vehicles

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "vehicles"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.vehicles_admin_vehicles_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Vehicles)
      end
    end
  end
end
