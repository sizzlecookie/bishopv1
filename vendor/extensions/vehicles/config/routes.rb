Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :vehicles do
    resources :vehicles, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :vehicles, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :vehicles, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
